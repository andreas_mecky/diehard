uniform extern float4x4 gWVP;
uniform extern texture gTex;

#define SAMPLE_COUNT 15

float2 SampleOffsets[SAMPLE_COUNT];
float SampleWeights[SAMPLE_COUNT];

sampler TexS = sampler_state {
	Texture = <gTex>;
	MinFilter = LINEAR;
	MagFilter = LINEAR;
	MipFilter = LINEAR;
	AddressU  = WRAP;
    AddressV  = WRAP;
};

struct OutputVS {
    float4 posH   : POSITION0;
	float2 Tex   : TEXCOORD0;
	float4 color0 : COLOR0;
};


OutputVS BlurVS(float3 posL : POSITION0,float2 tex0 : TEXCOORD0 , float4 color : COLOR0) {
    OutputVS outVS = (OutputVS)0;	
	outVS.posH = mul(float4(posL, 1.0f), gWVP);		
	outVS.Tex = tex0;
	outVS.color0 = color;
	return outVS;
}

float4 BlurPS(OutputVS input) : COLOR {
    float4 c = 0;    
    for (int i = 0; i < SAMPLE_COUNT; i++) {
        c += tex2D(TexS, input.Tex + SampleOffsets[i]) * SampleWeights[i];
    }    
    return c;
}

technique BlurTech {
	pass P0 {
		// A post process shader only needs a pixel shader.
		vertexShader = compile vs_2_0 BlurVS();
		pixelShader = compile ps_2_0 BlurPS();
	}
}