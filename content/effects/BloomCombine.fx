// This combines the bloom texture with the original scene texture.
// BloomIntensity, OriginalIntensity, BloomSaturation and OriginalSaturation is used
// to control the blooming effect.
// This shader is based on the example in creators.xna.com, where I learned this technique.
uniform extern float4x4 gWVP;
uniform extern texture gTex;
uniform extern texture ColorMap;
uniform extern texture gBlurTex;

sampler TexS = sampler_state {
  Texture = <gTex>;
  MinFilter = LINEAR;
  MagFilter = LINEAR;
  MipFilter = LINEAR;
  AddressU  = Clamp;
  AddressV  = Clamp;
};

// Create a sampler for the ColorMap texture using lianear filtering and clamping
sampler ColorMapSampler = sampler_state {
   Texture = <ColorMap>;
   MinFilter = Linear;
   MagFilter = Linear;
   MipFilter = Linear;   
   AddressU  = Clamp;
   AddressV  = Clamp;
};

// Controls the Intensity of the bloom texture
float BloomIntensity = 2.3;

// Controls the Intensity of the original scene texture
float OriginalIntensity = 1.0;

// Saturation amount on bloom
float BloomSaturation = 1.0;

// Saturation amount on original scene
float OriginalSaturation = 1.0;


float4 AdjustSaturation(float4 color, float saturation)
{
    // We define gray as the same color we used in the grayscale shader
    float grey = dot(color, float4(0.3, 0.59, 0.11,1.0));
    
    return lerp(grey, color, saturation);
}

struct OutputVS {
  float4 posH   : POSITION0;
  float2 Tex   : TEXCOORD0;
  float4 color0 : COLOR0;
};


OutputVS FadeVS(float3 posL : POSITION0,float2 tex0 : TEXCOORD0 , float4 color : COLOR0) {
  OutputVS outVS = (OutputVS)0; 
  outVS.posH = mul(float4(posL, 1.0f), gWVP);   
  outVS.Tex = tex0;
  outVS.color0 = color;
  return outVS;
}


float4 BCPS(OutputVS input) : COLOR0
{
	// Get our bloom pixel from bloom texture
	float4 bloomColor = tex2D(TexS, input.Tex);

	// Get our original pixel from ColorMap
	float4 originalColor = tex2D(ColorMapSampler, input.Tex);
    
    // Adjust color saturation and intensity based on the input variables to the shader
	bloomColor = AdjustSaturation(bloomColor, BloomSaturation) * BloomIntensity;
	originalColor = AdjustSaturation(originalColor, OriginalSaturation) * OriginalIntensity;
    
    // make the originalColor darker in very bright areas, avoiding these areas look burned-out
    originalColor *= (1 - saturate(bloomColor));
    
    // Combine the two images.
    return originalColor + bloomColor;
    //return tex2D(ColorMapSampler, input.Tex);
}


technique BCTech {
    pass P0 {
		// A post process shader only needs a pixel shader.
      vertexShader = compile vs_2_0 FadeVS();
      pixelShader = compile ps_2_0 BCPS();
    }
}
