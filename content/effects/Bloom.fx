uniform extern float4x4 gWVP;
uniform extern texture gTex;

// Get the threshold of what brightness level we want to glow
float Threshold = 0.25;

sampler TexS = sampler_state {
	Texture = <gTex>;
	MinFilter = LINEAR;
	MagFilter = LINEAR;
	MipFilter = LINEAR;
	//AddressU  = WRAP;
    //AddressV  = WRAP;
};

struct OutputVS {
    float4 posH   : POSITION0;
	float2 Tex   : TEXCOORD0;
	float4 color0 : COLOR0;
};


OutputVS FadeVS(float3 posL : POSITION0,float2 tex0 : TEXCOORD0 , float4 color : COLOR0) {
    OutputVS outVS = (OutputVS)0;	
	outVS.posH = mul(float4(posL, 1.0f), gWVP);		
	outVS.Tex = tex0;
	outVS.color0 = color;
	return outVS;
}

float4 BloomPS(OutputVS input) : COLOR0
{
    float4 Color = tex2D(TexS, input.Tex);
    
    // Get the bright areas that is brighter than Threshold and return it.
    return saturate((Color - Threshold) / (1 - Threshold));
}


technique BloomTech {
    pass P0 {
		// A post process shader only needs a pixel shader.
		vertexShader = compile vs_2_0 FadeVS();
        pixelShader = compile ps_2_0 BloomPS();
    }
}
