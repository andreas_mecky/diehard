uniform extern float4x4 gWVP;
uniform extern texture gTex;

const float PI = 3.14f;
float wave = PI / 0.75;                // pi/.75 is a good default
float distortion = 1;        // 1 is a good default
float2 centerCoord = float2(0.54,0.56);

sampler TexS = sampler_state {
	Texture = <gTex>;
	MinFilter = LINEAR;
	MagFilter = LINEAR;
	MipFilter = LINEAR;
	AddressU  = WRAP;
	AddressV  = WRAP;
};

struct OutputVS {
	float4 posH   : POSITION0;
	float2 tex0   : TEXCOORD0;
	float4 color0 : COLOR0;
};

OutputVS RippleVS(float3 posL : POSITION0,float2 tex0 : TEXCOORD0 , float4 color : COLOR0) {
	OutputVS outVS = (OutputVS)0;	
	outVS.posH = mul(float4(posL, 1.0f), gWVP);		
	outVS.tex0 = tex0;
	outVS.color0 = color;
	return outVS;
}

float4 RipplePS(OutputVS input) : COLOR {

	float2 distance = abs(input.tex0 - centerCoord);
    float scalar = length(distance);

    // invert the scale so 1 is centerpoint
    scalar = abs(1 - scalar);
        
    // calculate how far to distort for this pixel    
    float sinoffset = sin(wave / scalar);
    sinoffset = clamp(sinoffset, 0, 1);
    
    // calculate which direction to distort
    float sinsign = cos(wave / scalar);    
    
    // reduce the distortion effect
    sinoffset = sinoffset * distortion/32;
    
    // pick a pixel on the screen for this pixel, based on
    // the calculated offset and direction
    float4 clr = tex2D(TexS, texCoord+(sinoffset*sinsign));    

	//float4 clr = tex2D(TexS, input.tex0);
	//clr *= 0.5;
	return clr;
}

technique RippleTech {
	pass P0 {
		vertexShader = compile vs_2_0 RippleVS();
		pixelShader  = compile ps_2_0 RipplePS();
	}
};