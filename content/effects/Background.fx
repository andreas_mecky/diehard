// ===========================================
// Simple effect to texture the given object
// ===========================================

float4x4 gWVP;
texture gTex;
float3 position = float3(0,0,0);
float timer = 0.0;

sampler TexS = sampler_state {
	Texture = <gTex>;
	MinFilter = LINEAR;
	MagFilter = LINEAR;
	MipFilter = LINEAR;
	AddressU  = WRAP;
    AddressV  = WRAP;
};

struct OutputVS {
    float4 posH   : POSITION0;
	float2 tex0   : TEXCOORD0;
};


OutputVS TextureVS(float4 posL : POSITION0, float2 tex0: TEXCOORD0) {
    // Zero out our output.
	OutputVS outVS = (OutputVS)0;	
	// we are already in screen space
	outVS.posH = posL;
	outVS.tex0 = tex0; 
    return outVS;
}

float4 TexturePS(float2 texCoord : TEXCOORD0) : COLOR {    	
	texCoord.x += position.x * 0.00025f;
    texCoord.y += position.y * 0.00025f;
    texCoord *= 0.5f;
	float red = 0.7 + sin(timer) * 0.1;
	float green = 0.6 + sin(timer*3.0) * 0.3;
	float alpha = 1.0;//0.6 + sin(timer) * 0.05;
    float4 results = float4(red,green,red+0.2,alpha) * tex2D(TexS, texCoord);     
    return results;
}

technique BackgroundTech {

    pass P0 {
        // Specify the vertex and pixel shader associated with this pass.
        vertexShader = compile vs_2_0 TextureVS();
        pixelShader  = compile ps_2_0 TexturePS();
		//FillMode = Wireframe;
    }
}
