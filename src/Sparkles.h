#pragma once
#include <world\World.h>
#include "Constants.h"
#include <world\SpriteEntity.h>

const int MAX_SPARKLES = 128;

class Sparkles : public ds::GameObject {

struct Sparkle {

	int type;
	ds::SpriteEntity entity;
	ds::Vec2 velocity;
};

public:
	Sparkles(void);
	~Sparkles(void);
	void init();
	void update(float elapsed);
	void start(int type,const ds::Vec2& pos,int count);
	//int pickup(const ds::Vec2& pos,float radius);
	void setTargetPos(const ds::Vec2& tp) {
		m_TargetPos = tp;
	}
	void clear();
	void removeByID(int entityID);
private:
	ds::Vec2 m_TargetPos;
	//SparklePickup m_Pickups;
	int findFreeSparkle();
	GameSettings* m_GameSettings;
	Sparkle m_Sparkles[MAX_SPARKLES];
};

