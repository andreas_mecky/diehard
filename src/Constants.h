#pragma once
#include <math\math_types.h>
#include <utils\Color.h>
#include <settings\DynamicSettings.h>

struct GameSettings : public ds::DynamicSettings {

	float sparkleVelocity;
	float sparklePickupRadius;
	float sparkleTargetVelocity;
	// new stuff
	float bulletVelocity;
	float bulletTTL;
	int alienImpact;
	float readyTTL;

	void load(NewSettingsReader& reader) {
		reader.get<float>("sparkle_velocity",&sparkleVelocity);
		reader.get<float>("sparkle_pickup_radius",&sparklePickupRadius);
		reader.get<float>("sparkle_target_velocity",&sparkleTargetVelocity);

		reader.get<float>("bullet_velocity",&bulletVelocity);
		reader.get<float>("bullet_ttl",&bulletTTL);
		reader.get<int>("alien_impact",&alienImpact);
		reader.get<float>("ready_ttl",&readyTTL);
	}
};

struct HUDData {

	int scoreIdx;
	int score;
	int healthIdx;
	int health;
	int waveIdx;
	int waves;
	int sparkleIdx;
	int sparkles;

	void reset() {
		score = 0;
		health = 100;
		waves = 0;
		sparkles = 0;
	}

};

struct BombExplosion {

	ds::Vec2 position;
	float radius;
};

struct SwarmExplosion {

	ds::Vec2 position;
	int type;
};

// Collision types
const int BULLET_TYPE  = 1;
const int ALIEN_TYPE   = 2;
const int PLAYER_TYPE  = 3;
const int SPARKLE_TYPE = 4;