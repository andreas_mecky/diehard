#include "DieHard.h"
#include "utils\Log.h"
#include <math\math_types.h>
#include "Constants.h"
#include <renderer\shader.h>
#include <utils\font.h>
#include <math\GameMath.h>
#include <utils\Profiler.h>


ds::BaseApp *app = new DieHard(); 


DieHard::DieHard() : ds::BaseApp() {//, m_Allocator(64,256) , m_BlockAllocator(1024) {
	m_Width = 1024;
	m_Height = 768;
	m_ClearColor = ds::Color(0.0f,0.0f,0.0f,1.0f);		
	m_HeadPos = ds::Vec2(512,384);
}

DieHard::~DieHard() {
}

// -------------------------------------------------------
// Load content and prepare game
// -------------------------------------------------------
bool DieHard::loadContent() {	

	audio->loadSound("BombExplosion");
	audio->loadSound("Pickup");
	audio->loadSound("Explosion");

	renderer->createVertexBuffer(ds::PT_TRI,ds::VD_PTC,65536,true);
	renderer->createIndexBuffer(98304,true);

	m_AddBS = renderer->createBlendState(ds::BL_ONE,ds::BL_ONE,true);	

	m_World.setCamera(0,&m_Camera);
	//m_World.setCamera(5,&m_Camera);
	m_World.setCamera(7,&m_Camera);
	
	int id = m_World.createSpriteBatch("TextureArray");	
	//ds::font::load("xscale",renderer,m_World.getTextureID(id),m_Font);
	//int backID = m_World.createRenderTarget(0);
	//int bid = m_World.createSpriteBatch(backID,1024.0f,768.0f);	
	//int fadeShader = renderer->loadShader("fade","FadeTech");
	//m_FadeShader = renderer->getShader(fadeShader);
	//ds::shader::setTexture(m_FadeShader,"gTex",renderer,backID);
	//ds::shader::setFloat(m_FadeShader,"timer",0.0f);
	//m_FadeTimer = 0.0f;
	//int fadeMtrl = renderer->createMaterial("FadeMtrl",backID);
	//renderer->setShader(fadeMtrl,fadeShader);
	//int rid = m_World.createSpriteBatch(backID,1024.0f,768.0f);	
	m_World.loadSettings("game",&m_GameSettings);

	m_World.addSpriteEntity(0,id,&m_Background,800,600,ds::Rect(0,512,512,384),0.0f,3.125f,3.125f);

	m_World.addSpriteEntity(8,id,&m_HeadRingEntity,800,600,ds::Rect(170,0,100,100),0.0f,2.0f,2.0f,ds::Color(255,128,0,32));
	m_World.addSpriteEntity(8,id,&m_HeadEntity,400,400,ds::Rect(0,0,40,40));

	m_World.addCollisionEntity(8,&m_HeadCollisionEntity);
	m_World.setCircleShape(&m_HeadCollisionEntity,20.0f,PLAYER_TYPE);

	m_World.addSpriteEntity(8,id,&m_CursorEntity,400,400,ds::Rect(120,165,26,26));
	m_World.addSpriteEntity(10,id,&m_GameOverEntity,512,384,ds::Rect(620,0,480,60));
	m_GameOverEntity.setActive(false);
	m_World.addSpriteEntity(10,id,&m_ReadyEntity,512,384,ds::Rect(680,0,340,52));

	m_World.addTextEntity(10,id,"xscale",&m_TextEntity);
	m_TextEntity.addText(100,100,"Hello world");
	
	m_World.addHUDEntity(12,&m_HUD,id,"xscale");
	m_HUDData.scoreIdx = m_HUD.addCounter(450,730,6,0);
	m_HUDData.healthIdx = m_HUD.addCounter(100,730,3,100);
	m_HUDData.waveIdx = m_HUD.addCounter(900,730,3,0);
	m_HUDData.sparkleIdx = m_HUD.addCounter(480,30,4,0);
	m_HUDData.reset();

	m_HeadPos = ds::Vec2(400,400);

	int rt1 = m_World.createRenderTarget(0);//,512.0,384.0f);	
	m_World.addParticleSystemEntity(0,id,"small_exp",&m_Particles,2000,m_AddBS);
	m_World.addParticleSystemEntity(0,id,"ring_exp",&m_RingExp,2000,m_AddBS);
	m_World.addParticleSystemEntity(0,id,"bomb_exp",&m_BombExp,2000,m_AddBS);
	m_World.addParticleSystemEntity(0,id,"trail",&m_Trail,2000,m_AddBS);
	m_World.addParticleSystemEntity(0,id,"head_exp",&m_HeadExp,600,m_AddBS);
	m_World.addParticleSystemEntity(0,id,"bullet_exp",&m_BulletExp,600,m_AddBS);
	m_Trail.start(ds::Vec2(800,600));

	int oid = m_World.createSpriteBatch(rt1,1024.0f,768.0f,m_AddBS);	
	m_World.addSpriteEntity(6,oid,&m_OverlayEntity,512,384,ds::Rect(0,0,1024,768));
	m_OverlayEntity.setActive(false);
	/*
	// color map
	int rt2 = m_World.createRenderTarget(1);
	int psid = m_World.createSpriteBatch(rt1,512.0f,384.0f);
	m_World.addSpriteEntity(1,psid,&m_ParticleOverlayEntity,512,384,ds::Rect(0,0,512,384),0.0f,2.0f,2.0f);

	// Bloom
	int rt3 = m_World.createRenderTarget(2);
	int blid = m_World.createSpriteBatch(rt1,512.0f,384.0f);
	m_World.addSpriteEntity(2,blid,&m_BloomEntity,512,384,ds::Rect(0,0,512,384),0.0f,2.0f,2.0f);
	int bloomShaderID = ds::shader::createBloomShader(renderer,rt1,0.25f);
	m_World.setSpriteBatchShader(blid,bloomShaderID);

	// Bloom combine	
	int rt4 = m_World.createRenderTarget(3);
	int bcid = m_World.createSpriteBatch(rt4);
	m_World.addSpriteEntity(3,bcid,&m_BloomCombineEntity,512,384,ds::Rect(0,0,1024,768));
	int bcShaderID = renderer->loadShader("BloomCombine","BCTech");
	ds::Shader bcShader = renderer->getShader(bcShaderID);
	ds::shader::setTexture(bcShader,"gTex",renderer,rt3);
	ds::shader::setTexture(bcShader,"ColorMap",renderer,rt2);
	int bcMtrl = renderer->createMaterial("BCMtrl",rt4);
	renderer->setShader(bcMtrl,bcShaderID);
	m_World.setSpriteBatchShader(bcid,bcShaderID);

	int oid = m_World.createSpriteBatch(rt4,1024.0f,768.0f,m_AddBS);	
	m_World.addSpriteEntity(6,oid,&m_OverlayEntity,512,384,ds::Rect(0,0,1024,768));
	*/
	
	/*
	//                Name           Thresh  Blur Bloom  Base  BloomSat BaseSat
	new BloomSettings("Default",     0.25f,  4,   1.25f, 1,    1,       1),
	new BloomSettings("Soft",        0,      3,   1,     1,    1,       1),
	new BloomSettings("Desaturated", 0.5f,   8,   2,     1,    0,       1),
	new BloomSettings("Saturated",   0.25f,  4,   2,     1,    2,       0),
	new BloomSettings("Blurry",      0,      2,   1,     0.1f, 1,       1),
	new BloomSettings("Subtle",      0.5f,   2,   1,     1,    1,       1),
	*/
	m_World.loadSettings("bloom",&m_BloomSettings);
	m_World.createGameObject<BloomComponent>(&m_Bloom);
	m_Bloom.initialize(1,rt1,&m_BloomSettings,m_AddBS);
	m_Bloom.activate();
	
	m_World.createGameObject<Swarm>(&m_Swarm);
	m_World.createGameObject<Sparkles>(&m_Sparkles);
	m_World.createGameObject<GameTimer>(&m_ColorTimer);
	m_ColorTimer.stop();
	m_World.createGameObject<GameTimer>(&m_PlayTime);
	m_PlayTime.stop();
	m_World.createGameObject<GameTimer>(&m_ReadyTimer);
	m_ReadyTimer.stop();
	m_World.createGameObject<Bullets>(&m_Bullets);
	m_Bullets.setBulletExp(&m_BulletExp);

	m_World.createGameObject<Upgrades>(&m_Upgrades);

	m_World.addCollisionIgnore(PLAYER_TYPE,BULLET_TYPE);
	m_World.addCollisionIgnore(ALIEN_TYPE,SPARKLE_TYPE);
	m_World.addCollisionIgnore(BULLET_TYPE,SPARKLE_TYPE);

	/*
	BombExplosion* exp = MAKE_NEW(m_BlockAllocator,BombExplosion);
	m_BlockAllocator.debug();
	MAKE_DELETE(m_BlockAllocator,BombExplosion,exp);
	*/
	startGame();
	
	return true;
}


// -------------------------------------------------------
// Update
// -------------------------------------------------------
void DieHard::update(const ds::GameTime& gameTime) {
	
	PR_START("Fireworms-Update")
	ds::Vec2 mp = getMousePos();	
	mp.y = 768.0f - mp.y;	
	ds::Vec2 tp = m_HeadPos;
	m_CursorEntity.setPosition(mp);

	if ( m_Mode == GET_READY ) {
		if ( m_ReadyTimer.hasElapsed(m_GameSettings.readyTTL) ) {
			m_ReadyTimer.stop();
			m_ReadyEntity.setActive(false);
			m_Mode = RUNNING;
		}
	}

	if ( m_Mode == RUNNING || m_Mode == GET_READY ) {		
		ds::math::followRelative(mp,tp,&m_HeadAngle,2.0f,1.0f*gameTime.elapsed);	
		ds::math::clamp(tp,120.0f,850.0f,110.0f,660.0f);	
		m_HeadPos = tp;
		m_HeadEntity.setPosition(m_HeadPos);
		m_HeadEntity.setRotation(m_HeadAngle);
		m_HeadRingEntity.setPosition(m_HeadPos);
		m_Camera.setPosition(m_HeadPos,0.42f);
		m_WorldPos = m_Camera.transformToWorld(m_HeadPos);
		m_Swarm.setTargetPos(m_WorldPos);
		m_HeadCollisionEntity.setPosition(m_WorldPos);
		m_Sparkles.setTargetPos(m_WorldPos);
		//renderer->debug(10,10,ds::Color::WHITE,"wp %3.2f %3.2f angle %3.2f",m_WorldPos.x,m_WorldPos.y,RADTODEG(m_HeadAngle));
		
		ds::Vec2 trailPos = m_WorldPos;
		float tAngle = ds::math::reflect(m_HeadAngle) + DEGTORAD(180.0f);
		ds::vector::addRadial(trailPos,16.0f,tAngle);
		m_Trail.setEmitterPosition(trailPos);
		
		int upgrade = m_Upgrades.pickup(m_WorldPos,50.0f);
		if ( upgrade != -1 ) {
			LOG(logINFO) << "Player picked up upgrade: " << upgrade;
		}
	}

	if ( m_Mode == RUNNING ) {
		handleShooting(gameTime);

		if ( m_Swarm.numAlive() == 0 ) {
			startWave();
		}

		handleCollisions();

	}
	PR_END("Fireworms-Update")
	
}

// -------------------------------------------------------
// Handle collisions
// -------------------------------------------------------
void DieHard::handleCollisions() {
	bool pickedUP = false;
	if ( m_World.containsCollisions()) {
		int num = m_World.getCollisionCounter();
		LOG(logINFO) << "Caught collisions " << num;
		ds::Collision col;
		for ( int i = 0; i < num; ++i ) {
			m_World.getCollision(i,col);				
			if ( ds::coll::containsType(col,PLAYER_TYPE) && ds::coll::containsType(col,ALIEN_TYPE) ) {
				m_Swarm.removeByID(ds::coll::getIDByType(col,ALIEN_TYPE));
				m_RingExp.start(ds::coll::getPositionByType(col,ALIEN_TYPE));
				m_Particles.start(ds::coll::getPositionByType(col,ALIEN_TYPE));
				m_HUDData.health -= m_GameSettings.alienImpact;
				m_HUD.setCounterValue(m_HUDData.healthIdx,m_HUDData.health);
				audio->play("BombExplosion");
				if ( m_HUDData.health <= 0 ) {
					stopGame();
				}
			}
			if ( ds::coll::containsType(col,BULLET_TYPE) && ds::coll::containsType(col,ALIEN_TYPE) ) {
				m_Sparkles.start(0,ds::coll::getPositionByType(col,ALIEN_TYPE),4);
				m_Bullets.removeByID(ds::coll::getIDByType(col,BULLET_TYPE));
				m_Swarm.removeByID(ds::coll::getIDByType(col,ALIEN_TYPE));
				m_RingExp.start(ds::coll::getPositionByType(col,ALIEN_TYPE));
				m_Particles.start(ds::coll::getPositionByType(col,ALIEN_TYPE));
				m_HUDData.score += 50;
				m_HUD.setCounterValue(m_HUDData.scoreIdx,m_HUDData.score);
				audio->play("BombExplosion");
			}		
			if ( ds::coll::containsType(col,PLAYER_TYPE) && ds::coll::containsType(col,SPARKLE_TYPE) ) {
				++m_HUDData.sparkles;
				m_Sparkles.removeByID(ds::coll::getIDByType(col,SPARKLE_TYPE));
				pickedUP = true;
				audio->play("Pickup");
			}		
		}
	}
	if ( pickedUP ) {
		m_HUD.setCounterValue(m_HUDData.sparkleIdx,m_HUDData.sparkles);
	}
}

// -------------------------------------------------------
// Handle shooting
// -------------------------------------------------------
void DieHard::handleShooting( const ds::GameTime &gameTime ) {
	if ( m_Firing ) {
		m_BulletTimer += gameTime.elapsed;
		if ( m_BulletTimer > m_GameSettings.bulletTTL ) {
			m_BulletTimer = 0.0f;
			if ( m_Weapon.type == WP_SINGLE ) {
				m_Bullets.start(m_WorldPos,m_HeadAngle);
			}
			else if ( m_Weapon.type == WP_DOUBLE ) {				
				m_Weapon.doubleTimer += gameTime.elapsed;
				// FIXME: take from game settings
				if ( m_Weapon.doubleTimer > 5.0f ) {
					m_Weapon.type = WP_SINGLE;
				}
				float ba = ds::math::reflect(m_HeadAngle);
				ds::Vec2 fp = m_WorldPos;
				ds::vector::addRadial(fp,8.0f,ba+DEGTORAD(90.0f));
				m_Bullets.start(fp,m_HeadAngle);
				ds::Vec2 sp = m_WorldPos;
				ds::vector::addRadial(sp,8.0f,ba-DEGTORAD(90.0f));
				m_Bullets.start(sp,m_HeadAngle);
			}
		}
	}
}

// -------------------------------------------------------
// Stop game
// -------------------------------------------------------
void DieHard::stopGame() {
	LOG(logINFO) << "Player got killed";
	//m_CollisionManager.remove(m_ColID);
	m_Mode = GAMEOVER;
	audio->play("Explosion",100);
	m_GameOverEntity.setActive(true);
	m_HeadEntity.setActive(false);
	m_HeadCollisionEntity.setActive(false);
	m_HeadRingEntity.setActive(false);
	m_Trail.stop();
	m_Trail.setActive(false);
	m_HeadExp.start(m_WorldPos);
	m_Sparkles.clear();
	// explode all bombs
	std::vector<ds::Vec2> positions;
	positions.clear();
	// explode all aliens
	m_Swarm.getActivePosition(positions);
	for ( size_t i = 0; i < positions.size(); ++i ) {
		m_Particles.start(positions[i]);
		m_RingExp.start(positions[i]);
	}
	m_ColorTimer.stop();
	m_PlayTime.stop();
	LOG(logINFO) << "play time " << m_PlayTime.getMinutes() << ":" << m_PlayTime.getSeconds();
	m_Swarm.clear();
}

// -------------------------------------------------------
// Start game
// -------------------------------------------------------
void DieHard::startGame() {
	m_Swarm.clear();
	//m_ColID = m_CollisionManager.add(m_WorldPos,20.0f,PLAYER_TYPE);
	m_GameOverEntity.setActive(false);
	m_HeadPos = ds::Vec2(512,384);
	m_Camera.setPosition(m_HeadPos,0.42f);
	m_WorldPos = m_Camera.transformToWorld(m_HeadPos);
	m_Trail.start(m_WorldPos);
	m_Trail.setActive(true);	
	m_HeadEntity.setActive(true);
	m_HeadCollisionEntity.setActive(true);
	m_HeadRingEntity.setActive(true);	
	m_Sparkles.clear();
	m_ColorTimer.reset();
	m_ColorTimer.start();
	m_PlayTime.reset();
	m_PlayTime.start();
	m_Firing = false;
	m_BulletTimer = 0.0f;
	m_HUDData.reset();
	m_HUD.setCounterValue(m_HUDData.healthIdx,m_HUDData.health);
	m_HUD.setCounterValue(m_HUDData.scoreIdx,m_HUDData.score);
	m_HUD.setCounterValue(m_HUDData.waveIdx,m_HUDData.waves);
	m_ReadyEntity.setActive(true);
	m_ReadyTimer.reset();
	m_ReadyTimer.start();
	m_Weapon.reset();
	m_Mode = GET_READY;
}

// -------------------------------------------------------
// Start wave
// -------------------------------------------------------
void DieHard::startWave() {
	int tmp[4];
	for ( int i = 0; i < 4; ++i ) {
		tmp[i] = i + 1;
	}	
	for ( int i = 4 - 1; i > 0 ; --i ) {
		int idx = ds::math::random(0,3);
		int current = tmp[i];
		tmp[i] = tmp[idx];
		tmp[idx] = current;
	}
	m_HUDData.waves += 2;
	m_HUD.setCounterValue(m_HUDData.waveIdx,m_HUDData.waves);
	m_Swarm.start(tmp[0],0,12);
	m_Swarm.start(tmp[1],0,12);
}

void DieHard::OnChar( char ascii,unsigned int keyState ) {	
	if ( ascii == 'r' ) {
		if ( m_Mode != RUNNING ) {
			startGame();
		}
	}
	if ( ascii == 'a' ) {
		m_Particles.start(ds::Vec2(800,600));
		m_RingExp.start(ds::Vec2(800,600));
	}
	if ( ascii == 'b' ) {		
		m_HeadExp.start(ds::Vec2(800,600));
	}
	if ( ascii == '1' ) {		
		m_Upgrades.activate(0);
	}
	if ( ascii == '2' ) {		
		m_Upgrades.activate(1);
	}
	if ( ascii == '3' ) {		
		m_Upgrades.activate(2);
	}
	
	if ( ascii == 'q' ) {
		m_World.debug();
	}
	
	if ( ascii == 'c' ) {
		m_World.togglePause();
	}
	if ( ascii == 't' ) {
		m_OverlayEntity.setActive(false);
		m_Bloom.activate();
	}
	if ( ascii == 'u' ) {
		m_OverlayEntity.setActive(true);
		m_Bloom.deactivate();
	}
}

// -------------------------------------------------------
// Button down
// -------------------------------------------------------
void DieHard::OnButtonDown( int button,int x,int y ) {
	m_Firing = true;
}

// -------------------------------------------------------
// Button Up
// -------------------------------------------------------
void DieHard::OnButtonUp( int button,int x,int y ) {
	m_Firing = false;
}


