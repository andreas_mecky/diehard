#pragma once
#include <world\World.h>
#include <renderer\render_types.h>
#include <world\SpriteEntity.h>

// -------------------------------------------------------
// Alien
// -------------------------------------------------------
struct Alien {

	int id;
	ds::SpriteEntity entity;
	bool active;
	ds::Vec2 velocity;
	ds::Vec2 position;
	int type;
	float timer;
};

const int MAX_SWARM = 256;
// -------------------------------------------------------
// Swarm
// -------------------------------------------------------
class Swarm : public ds::GameObject {

public:
	Swarm();
	virtual ~Swarm(void);
	void setTargetPos(const ds::Vec2& targetPos) {
		m_TargetPos = targetPos;
	}
	void update(float elapsed);
	void init();
	int numAlive();
	void start(int type,int pos,int count);
	void getActivePosition(std::vector<ds::Vec2>& positions);
	void clear();
	void removeByID(int entityID);
private:
	Alien* findClosest(const Alien& currentAlien);
	int findFreeAlien();
	Alien m_Aliens[MAX_SWARM];
	Alien* m_Target;	
	ds::Vec2 m_TargetPos;
};

