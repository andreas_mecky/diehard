#pragma once
#include <world\World.h>
#include <world\SpriteEntity.h>
#include <renderer\render_types.h>

const int SAMPLE_COUNT = 15;

struct BloomSettings : public ds::DynamicSettings {

	float bloomThreshold;
	float bloomIntensity;
	float originalIntensity;
	float bloomSaturation;
	float originalSaturation;
	float blurAmount;

	void load(NewSettingsReader& reader) {
		reader.get<float>("bloom_threshold",&bloomThreshold);
		reader.get<float>("bloom_intensity",&bloomIntensity);
		reader.get<float>("original_intensity",&originalIntensity);
		reader.get<float>("bloom_saturation",&bloomSaturation);
		reader.get<float>("original_saturation",&originalSaturation);
		reader.get<float>("blur_amount",&blurAmount);
	}
};

class BloomComponent : public ds::GameObject {

public:
	BloomComponent(void);
	~BloomComponent(void);
	void update(float elapsed);
	void init() {}
	void initialize(int startLayer,int baseTexture,BloomSettings* bloomSettings,int blendState = -1);
	void activate() {
		setState(true);
	}
	void deactivate() {
		setState(false);
	}
private:
	void setState(bool active);
	void setBlurEffectParameters(ds::Shader& shader,float dx, float dy);
	float computeGaussian(float n);
	BloomSettings* m_Settings;
	ds::SpriteEntity m_BloomEntity;
	ds::SpriteEntity m_BlurHEntity;
	ds::SpriteEntity m_BlurVEntity;
	ds::SpriteEntity m_BloomCombineEntity;
	ds::SpriteEntity m_OverlayEntity;
	ds::Shader m_BlurHShader;
	ds::Shader m_BlurVShader;
	ds::Shader m_BloomShader;
	ds::Shader m_BloomCombineShader;
};

