#pragma once
#include "Allocator.h"
#include "PoolAllocator.h"

class BlockAllocator : public Allocator {

struct AllocHandler {

	PoolAllocator* allocator;
	u32 size;
};
public:
	BlockAllocator(u32 blockSize);
	virtual ~BlockAllocator();

	void* allocate(u32 size, u8 alignment);

	void deallocate(void* p);

	void debug();
private:
	AllocHandler m_Handler[7];
};

