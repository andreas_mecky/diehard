#pragma comment(lib, "Diesel2.lib")
#pragma comment(lib, "dinput8.lib")
#pragma comment(lib, "dsound.lib")
#pragma comment(lib, "dxerr.lib")
#pragma warning(disable : 4995)

#pragma once
#include "base\BaseApp.h"
#include "dxstdafx.h"
#include <renderer\render_types.h>
#include <nodes\SpriteBatch.h>
#include <world\World.h>
#include "Swarm.h"
#include "Sparkles.h"
#include <world\HUDEntity.h>
#include <world\TextEntity.h>
#include "GameTimer.h"
#include "Bullets.h"
#include "PoolAllocator.h"
#include "BlockAllocator.h"
#include "Upgrades.h"
#include "BloomComponent.h"

class DieHard : public ds::BaseApp {

enum GameMode {
	GET_READY,
	RUNNING,
	GAMEOVER
};

enum WeaponType {
	WP_SINGLE,
	WP_DOUBLE,
	WP_TRIPLE
};

struct Weapon {

	WeaponType type;
	float doubleTimer;
	float tripleTimer;

	Weapon() : type(WP_SINGLE) , doubleTimer(0.0f) , tripleTimer(0.0f) {}

	void reset() {
		type = WP_SINGLE;
		doubleTimer = 0.0f;
		tripleTimer = 0.0f;
	}
};

public:	
	DieHard();
	virtual ~DieHard();
	bool loadContent();
	const char* getTitle() {
		return "DieHard";
	}
	void update(const ds::GameTime& gameTime);

	void handleCollisions();
	void handleShooting( const ds::GameTime &gameTime ); 	
	void draw(const ds::GameTime& gameTime) {}
private:
	void stopGame();
	void startGame();
	void startWave();
	virtual void OnChar( char ascii,unsigned int keyState );
	void OnButtonDown( int button,int x,int y );
	void OnButtonUp( int button,int x,int y );

	GameSettings m_GameSettings;
	ds::SpriteEntity m_Background;
	ds::SpriteEntity m_HeadEntity;
	ds::SpriteEntity m_HeadRingEntity;
	ds::SpriteEntity m_CursorEntity;
	ds::SpriteEntity m_GameOverEntity;
	ds::SpriteEntity m_ReadyEntity;
	//ds::SpriteEntity m_RippleEntity;
	//ds::ScreenOverlayEntity m_OverlayEntity;
	ds::SpriteEntity m_OverlayEntity;
	ds::SpriteEntity m_ParticleOverlayEntity;
	//ds::SpriteEntity m_BloomEntity;
	//ds::SpriteEntity m_Blur2Entity;
	//ds::SpriteEntity m_BloomCombineEntity;
	ds::ParticlesystemEntity m_Particles;
	ds::ParticlesystemEntity m_RingExp;
	ds::ParticlesystemEntity m_BombExp;
	ds::ParticlesystemEntity m_Trail;
	ds::ParticlesystemEntity m_HeadExp;
	ds::ParticlesystemEntity m_BulletExp;
	ds::CollisionEntity m_HeadCollisionEntity;
	ds::HUDEntity m_HUD;
	ds::TextEntity m_TextEntity;
	HUDData m_HUDData;
	Upgrades m_Upgrades;
	Sparkles m_Sparkles;
	Bullets m_Bullets;
	BloomComponent m_Bloom;
	float m_BulletTimer;
	bool m_Firing;
	ds::Vec2 m_HeadPos;
	float m_HeadAngle;
	//int m_ColID;
	ds::Camera2D m_Camera;
	Swarm m_Swarm;
	uint32 m_AddBS;
	ds::Vec2 m_WorldPos;
	GameMode m_Mode;
	GameTimer m_ColorTimer;
	GameTimer m_ReadyTimer;
	GameTimer m_PlayTime;
	Weapon m_Weapon;

	BloomSettings m_BloomSettings;

	ds::Shader m_FadeShader;
	float m_FadeTimer;
	//PoolAllocator m_Allocator;
	//BlockAllocator m_BlockAllocator;
};