#include "GameTimer.h"

// -------------------------------------------------------
// Update
// -------------------------------------------------------
void GameTimer::update(float elapsed) {
	if ( isActive() ) {
		m_Total += elapsed;
		m_Timer += elapsed;
		m_SecTimer += elapsed;
		if ( m_SecTimer >= 1.0f ) {
			m_SecTimer = 0.0f;
			++m_Seconds;
			if ( m_Seconds == 60 ) {
				++m_Minutes;
				m_Seconds = 0;
			}		
			if ( m_Minutes == 60 ) {
				++m_Hours;
				m_Minutes = 0;
			}
		}
	}
}

// -------------------------------------------------------
// Has elapsed
// -------------------------------------------------------
bool GameTimer::hasElapsed(float threshold,bool reset) {
	if ( m_Timer >= threshold ) {
		if ( reset ) {
			m_Timer = 0.0f;
		}
		return true;
	}
	return false;
}