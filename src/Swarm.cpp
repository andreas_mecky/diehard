#include "Swarm.h"
#include "Constants.h"

Swarm::Swarm() : GameObject() {
}

Swarm::~Swarm(void) {}

// -------------------------------------------------------
// Init
// -------------------------------------------------------
void Swarm::init() {
	m_Target = new Alien;
	ds::Vec2 initialPos(400,200);
	for ( int i = 0; i < MAX_SWARM; ++i ) {
		Alien* alien = &m_Aliens[i];
		alien->id = i;
		m_World->addSpriteEntity(0,"blob1",0,&alien->entity);		
		alien->entity.setPosition(initialPos);
		alien->position = alien->entity.getPosition();		
		alien->active = false;
		alien->entity.setActive(false);
		alien->type = -1;
		m_World->setCircleShape(&alien->entity,20.0f,ALIEN_TYPE);
	}	
}

// -------------------------------------------------------
// Start
// -------------------------------------------------------
void Swarm::start(int type,int pos,int count) {
	ds::Vec2 p(400,170);
	int steps = 1000 / count;
	float sx = 1200.0f - steps * count;
	int stepsY = 750 / count;
	float sy = 170.0f;
	if ( type == 3 ) {
		sy = 900.0f;
	}
	if ( type == 2 ) {
		sx = 1180.0f;
	}
	if ( type == 4 ) {
		sx = 190.0f;
	}
	int idx = -1;
	for ( int i = 0; i < count; ++i ) {
		idx = findFreeAlien();
		if ( idx != -1 ) {
			Alien* alien = &m_Aliens[idx];
			alien->type = ds::math::random(0,3);
			if ( type == 1 || type == 3 ) {
				p.x = sx + i * steps;
				p.y = sy;
			}
			if ( type == 2 || type == 4 ) {
				p.x = sx;
				p.y = sy + i * steps;
			}
			alien->entity.setPosition(p);
			alien->position = alien->entity.getPosition();			
			alien->active = true;			
			int vel = ds::math::random(120,160);
			if ( type == 1 ) {
				alien->velocity = ds::Vec2(0,vel);
			}
			if ( type == 2 ) {
				alien->velocity = ds::Vec2(-vel,0);
			}
			if ( type == 3 ) {
				alien->velocity = ds::Vec2(0,-vel);
			}
			if ( type == 4 ) {
				alien->velocity = ds::Vec2(vel,0);
			}
			alien->entity.setActive(true);
			alien->timer = 0.0f;
		}
	}		
}

// -------------------------------------------------------
// Remove by collision ID
// -------------------------------------------------------
void Swarm::removeByID(int entityID) {
	for ( int i = 0; i < MAX_SWARM; ++i ) {
		Alien* alien = &m_Aliens[i];
		if ( alien->active && alien->entity.getID() == entityID ) {
			alien->active = false;
			alien->entity.setActive(false);
		}
	}
}
// -------------------------------------------------------
// Clear
// -------------------------------------------------------
void Swarm::clear() {
	for ( int i = 0; i < MAX_SWARM; ++i ) {
		Alien* alien = &m_Aliens[i];
		alien->active = false;
		alien->entity.setActive(false);
	}
}
// -------------------------------------------------------
// Find free alien
// -------------------------------------------------------
int Swarm::findFreeAlien() {
	for ( int i = 0; i < MAX_SWARM; ++i ) {
		Alien* alien = &m_Aliens[i];
		if ( !alien->active ) {
			return i;
		}
	}
	return -1;
}

// -------------------------------------------------------
// Get active positions
// -------------------------------------------------------
void Swarm::getActivePosition(std::vector<ds::Vec2>& positions) {
	for ( int i = 0; i < MAX_SWARM; ++i ) {
		Alien* alien = &m_Aliens[i];
		if ( alien->active ) {
			positions.push_back(alien->position);
		}
	}
}
// -------------------------------------------------------
// numAlive
// -------------------------------------------------------
int Swarm::numAlive() {
	int ret = 0;
	for ( int i = 0; i < MAX_SWARM; ++i ) {
		Alien* alien = &m_Aliens[i];
		if ( alien->active ) {
			++ret;
		}
	}
	return ret;
}

// -------------------------------------------------------
// Update
// -------------------------------------------------------
void Swarm::update(float elapsed) {
	m_Target->position = m_TargetPos;

	for ( int i = 0; i < MAX_SWARM; ++i ) {
		Alien* alien = &m_Aliens[i];
		if ( alien->active ) {
			ds::vector::addScaled(alien->position,alien->velocity,elapsed);
			if ( alien->position.x < 170.0f || alien->position.x > 1200.0f || alien->position.y < 160.0f || alien->position.y > 940.0f ) {
				alien->active = false;
				alien->entity.setActive(false);				
			}
			alien->timer += elapsed * ds::math::random(0.8f,1.2f);
			alien->entity.setRotation(alien->timer);
			alien->entity.setPosition(alien->position);			
		}
	}
}

