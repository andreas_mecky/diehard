#pragma once
#include <assert.h>

typedef unsigned int u32;
typedef unsigned char u8;

class Allocator {

public:
	Allocator() : m_UsedMemory(0) , m_NumAllocations(0) {        
	}
	virtual ~Allocator() {
		assert(m_UsedMemory == 0);
		assert(m_NumAllocations == 0);
	}
	virtual void* allocate(u32 size, u8 alignment) = 0;

	virtual void deallocate(void* p) = 0;

	const u32 getUsedMemory() const {
		return m_UsedMemory;
	}
	const u32 getNumAllocations() const {
		return m_NumAllocations;
	}
protected:
	u32 m_UsedMemory;
	u32 m_NumAllocations;
};

/// Creates a new object of type T using the allocator a to allocate the memory.
#define MAKE_NEW(a, T, ...)             (new ((a).allocate(sizeof(T), 0)) T(__VA_ARGS__))

/// Frees an object allocated with MAKE_NEW.
#define MAKE_DELETE(a, T, p)    do {if (p) {(p)->~T(); a.deallocate(p);}} while (0)

