#include "PoolAllocator.h"
#include <utils\Log.h>

PoolAllocator::PoolAllocator(u8 blockSize,u32 elements) : Allocator() , m_BlockSize(blockSize) , m_MaxElements(elements) {
	m_Data = new u8[blockSize * elements];
	m_End = m_Data + blockSize * elements;
	m_Indices = new int[elements];
	for ( int i = 0; i < elements; ++i ) {
		m_Indices[i] = -1;
	}
}

PoolAllocator::~PoolAllocator() {
	delete m_Data;
}

void* PoolAllocator::allocate(u32 size, u8 alignment) {
	int idx = findFreeIndex();
	if ( idx != -1 ) {
		m_Indices[idx] = 1;
		++m_NumAllocations;
		m_UsedMemory += m_BlockSize;
		void* data = m_Data + idx * m_BlockSize;
		return data;
	}
	return 0;
}

void PoolAllocator::deallocate(void* p) {
	int idx = 0;
	u8* data = m_Data;
	while ( p > data && idx < m_MaxElements ) {
		++idx;
		data += m_BlockSize;
	}
	m_Indices[idx] = -1;
	--m_NumAllocations;
	m_UsedMemory -= m_BlockSize;
}

bool PoolAllocator::contains(void* p) {
	return ( p >= m_Data && p <= m_End);	
}


int PoolAllocator::findFreeIndex() {
	for ( int i = 0; i < m_MaxElements; ++i ) {
		if ( m_Indices[i] == -1 ) {
			return i;
		}
	}
	return -1;
}

void PoolAllocator::debug() {
	LOG(logINFO) << "Allocations: " << m_NumAllocations;
	LOG(logINFO) << "Used memory: " << m_UsedMemory;
	if ( m_NumAllocations > 0 ) {
		LOG(logINFO) << "Blocks:";
		for ( int i = 0; i < m_MaxElements; ++i ) {
			if ( m_Indices[i] != -1 ) {
				LOG(logINFO) << "Used: " << i;
			}
		}
	}
}