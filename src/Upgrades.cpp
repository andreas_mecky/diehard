#include "Upgrades.h"


Upgrades::Upgrades(void) : ds::GameObject() {
}


Upgrades::~Upgrades(void) {
}

// -------------------------------------------------------
// Init
// -------------------------------------------------------
void Upgrades::init() {
	for ( int i = 0; i < 3; ++i ) {
		UpgradeItem* item = &m_Items[i];
		m_World->addSpriteEntity(5,0,&item->entity,700,600,ds::Rect(280,i * 46,46,46));
		m_World->addSpriteEntity(5,0,&item->border,700,600,ds::Rect(280,138,46,46));
		item->entity.setActive(false);
		item->border.setActive(false);
		item->active = false;
		item->ttl = 2.0f;
	}	
}

// -------------------------------------------------------
// Activate
// -------------------------------------------------------
void Upgrades::activate(int idx) {
	UpgradeItem* item = &m_Items[idx];
	if ( item->active ) {
		item->timer = 0.0f;
	}
	else {
		item->timer = 0.0f;
		item->entity.setActive(true);
		float x = ds::math::random(400,800);
		float y = ds::math::random(300,700);
		item->entity.setPosition(ds::Vec2(x,y));
		item->border.setPosition(item->entity.getPosition());
		item->border.setActive(true);
		item->active = true;
	}
}

// -------------------------------------------------------
// Update
// -------------------------------------------------------
void Upgrades::update(float elapsed) {
	for ( int i = 0; i < 3; ++i ) {
		UpgradeItem* item = &m_Items[i];
		if ( item->active ) {
			item->timer += elapsed;
			float angle = item->timer / item->ttl * 2.0f * ds::PI * 4.0f;
			float scale = 0.9f + sin(angle) * 0.1f;
			item->border.setScale(scale,scale);
			if ( item->timer > item->ttl ) {
				item->entity.setActive(false);
				item->border.setActive(false);
				item->active = false;
			}
		}
	}
}

// -------------------------------------------------------
// pickup
// -------------------------------------------------------
int Upgrades::pickup(const ds::Vec2& pos,float radius) {
	int points = -1;
	float dist = 0.0f;
	ds::Vec2 pen(0,0);		
	for ( int i = 0; i < 3; ++i ) {
		UpgradeItem* item = &m_Items[i];
		if ( item->active ) {			
			if ( ds::math::checkCircleIntersection(item->entity.getPosition(),20.0f,pos,radius,&dist,pen)) {
				points = i;				
				item->entity.setActive(false);
				item->border.setActive(false);
				item->active = false;
			}
		}
	}
	return points;
}