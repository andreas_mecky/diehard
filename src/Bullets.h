#pragma once
#include <world\World.h>
#include "Constants.h"
#include <world\SpriteEntity.h>

const int MAX_BULLETS = 32;

class Bullets : public ds::GameObject {

struct Bullet {
	ds::SpriteEntity entity;
	int id;
	bool active;
	ds::Vec2 position;
	ds::Vec2 velocity;
	float angle;
	int colID;
};

public:
	Bullets() : ds::GameObject() {}
	virtual ~Bullets() {}
	void init();
	void update(float elapsed);
	void start(const ds::Vec2& pos,float angle);
	void removeByID(int entityID);
	void setBulletExp(ds::ParticlesystemEntity* bulletExp) {
		m_BulletExp = bulletExp;
		m_Emitter = m_BulletExp->getEmitter<ds::ConeEmitter>();
	}
private:
	void startExp(Bullet* bullet);
	void startExp(const ds::Vec2& pos,float startAngle,float endAngle);
	int findFreeBullet();	
	ds::ConeEmitter* m_Emitter;
	ds::ParticlesystemEntity* m_BulletExp;
	Bullet m_Bullets[MAX_BULLETS];
	GameSettings* m_GameSettings;
};

