#include "BlockAllocator.h"
#include <utils\Log.h>


BlockAllocator::BlockAllocator(u32 blockSize) {
	int bs = 4;
	for ( int i = 0; i < 7; ++i ) {
		AllocHandler* handler = &m_Handler[i];
		LOG(logINFO) << "Creating new PA - size " << bs;
		handler->allocator = new PoolAllocator(bs,blockSize);
		handler->size = bs;
		bs = bs << 1;
	}
}


BlockAllocator::~BlockAllocator(void) {
	for ( int i = 0; i < 7; ++i ) {
		AllocHandler* handler = &m_Handler[i];
		delete handler->allocator;
	}
}

void* BlockAllocator::allocate(u32 size, u8 alignment) {
	assert(size <= 256);
	LOG(logINFO) << "requesting alloc for " << size;
	int idx = 0;
	AllocHandler* handler = &m_Handler[0];
	while ( size > handler->size && idx < 7 ) {
		++idx;
		handler = &m_Handler[idx];
	}
	LOG(logINFO) << "found handler at " << idx << " with size: " << handler->size;
	return handler->allocator->allocate(size,alignment);
}

void BlockAllocator::deallocate(void* p) {
	for ( int i = 0; i < 7; ++i ) {
		AllocHandler* handler = &m_Handler[i];
		if ( handler->allocator->contains(p)) {
			LOG(logINFO) << "Found matching handler - size: " << handler->size;
			handler->allocator->deallocate(p);
		}
	}
}

void BlockAllocator::debug() {
	for ( int i = 0; i < 7; ++i ) {
		AllocHandler* handler = &m_Handler[i];
		LOG(logINFO) << "Handler - size: " << handler->size;
		handler->allocator->debug();
	}
}