#pragma once
#include <world\World.h>
#include <world\SpriteEntity.h>

class Upgrades : public ds::GameObject {

struct UpgradeItem {
	ds::SpriteEntity entity;
	ds::SpriteEntity border;
	float timer;
	float ttl;
	bool active;
};

public:
	Upgrades(void);
	virtual ~Upgrades(void);
	void update(float elapsed);
	void init();
	void activate(int idx);
	int pickup(const ds::Vec2& pos,float radius);
private:
	UpgradeItem m_Items[3];	
};

