#include "Bullets.h"

// ----------------------------------------------------
// Init
// ----------------------------------------------------
void Bullets::init() {
	m_GameSettings = static_cast<GameSettings*>(m_World->getSettings("game"));	
	for ( int i = 0; i < MAX_BULLETS; ++i ) {
		Bullet* b = &m_Bullets[i];
		b->active = false;
		b->id = i;
		m_World->addSpriteEntity(0,"bullet",0,&b->entity);
		b->entity.setActive(false);
		m_World->setCircleShape(&b->entity,5.0f,BULLET_TYPE);
	}
}

// ----------------------------------------------------
// Start bullet
// ----------------------------------------------------
void Bullets::start(const ds::Vec2& pos,float angle) {
	int idx = findFreeBullet();
	if ( idx != -1 ) {
		float myAngle = ds::math::reflect(angle);
		Bullet* b = &m_Bullets[idx];
		b->active = true;
		b->entity.setActive(true);
		b->entity.setPosition(pos);
		b->velocity = ds::math::getRadialVelocity(RADTODEG(myAngle),m_GameSettings->bulletVelocity);
		b->entity.setRotation(angle);
		b->angle = angle;
	}
}

// ----------------------------------------------------
// Find free bullet
// ----------------------------------------------------
int Bullets::findFreeBullet() {
	for ( int i = 0; i < MAX_BULLETS; ++i ) {
		Bullet* b = &m_Bullets[i];
		if ( !b->active ) {
			return i;
		}
	}
	return -1;
}

// ----------------------------------------------------
// Update
// ----------------------------------------------------
void Bullets::update(float elapsed) {
	for ( int i = 0; i < MAX_BULLETS; ++i ) {
		Bullet* b = &m_Bullets[i];
		if ( b->active ) {
			ds::Vec2 p = b->entity.getPosition();
			ds::vector::addScaled(p,b->velocity,elapsed);
			b->entity.setPosition(p);
			b->position = p;
			bool kill = false;
			if ( p.x < 160.0f ) {
				kill = true;
				startExp(p,270.0f,90.0f);
			}
			else if ( p.x > 1220.0f ) {
				kill = true;
				startExp(p,90.0f,270.0f);
			}
			else if ( p.y < 150.0f ) {
				kill = true;
				startExp(p,0.0f,180.0f);
			}
			else if ( p.y > 950.0f ) {
				kill = true;
				startExp(p,180.0f,360.0f);
			}
			if ( kill ) {
				b->active = false;
				b->entity.setActive(false);
			}
		}
	}
}

// ----------------------------------------------------
// Start Bullet explosion
// ----------------------------------------------------
void Bullets::startExp(Bullet* bullet) {
	float angle = ds::math::reflect(bullet->angle);
	angle = RADTODEG(angle) - 180.0f;
	float startAngle = angle - 90.0f;
	float endAngle = angle + 90.0f;	
	startExp(bullet->position,startAngle,endAngle);	
}

// ----------------------------------------------------
// Start Bullet explosion
// ----------------------------------------------------
void Bullets::startExp(const ds::Vec2& pos,float startAngle,float endAngle) {	
	m_Emitter->setAngle(startAngle,endAngle);
	m_BulletExp->start(pos);
}

// ----------------------------------------------------
// Remove by collision ID
// ----------------------------------------------------
void Bullets::removeByID(int entityID) {
	for ( int i = 0; i < MAX_BULLETS; ++i ) {
		Bullet* b = &m_Bullets[i];
		if ( b->entity.getID() == entityID ) {
			b->active = false;
			b->entity.setActive(false);
			startExp(b);
		}
	}
}
