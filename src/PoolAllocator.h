#ifndef POOLALLOCATOR_H
#define	POOLALLOCATOR_H
#include "Allocator.h"

class PoolAllocator : public Allocator {

public:
	PoolAllocator(u8 blockSize,u32 elements);    
	virtual ~PoolAllocator();
	void* allocate(u32 size, u8 alignment);    
	void deallocate(void* p);
	bool contains(void* p);
	void debug();
	const u8 getBlockSize() const {
		return m_BlockSize;
	}
	const u32 getMaxElements() const {
		return m_MaxElements;
	}
private:
	int findFreeIndex();
	PoolAllocator(const PoolAllocator& orig) {}
	u32 m_MaxElements;
	u8 m_BlockSize;
	u8* m_Data;
	u8* m_End;
	int* m_Indices;
};

#endif	/* POOLALLOCATOR_H */